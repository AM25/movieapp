//
//  ViewController.h
//  movieApp
//
//  Created by Arnita Martiana on 08/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>{
    NSArray *dataColl;
    NSArray *dataArray;
    NSArray *curr;
    NSMutableArray *dataResponse;
    UIRefreshControl *mRefreshControl;
}
- (IBAction)btnMore:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewMovie;
@property (weak, nonatomic) IBOutlet UICollectionView *mColl;
@property (weak, nonatomic) IBOutlet UIView *mView;
- (IBAction)btnPopuler:(id)sender;
- (IBAction)btnTop:(id)sender;

- (IBAction)btnNext:(id)sender;

@end

