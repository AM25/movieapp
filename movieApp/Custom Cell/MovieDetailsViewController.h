//
//  MovieDetailsViewController.h
//  movieApp
//
//  Created by Arnita Martiana on 08/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieDetailsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>{
    
    @public
    NSArray *dataDetails;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgMovieDetail;
@property (strong, nonatomic) NSArray *theData;
@property (weak, nonatomic) IBOutlet UIButton *mBtnBuy;
@property (weak, nonatomic) IBOutlet UIButton *mBtnBack;
@property (weak, nonatomic) IBOutlet UILabel *lbRate;

@property (weak, nonatomic) IBOutlet UILabel *lblMovieName;
@property (weak, nonatomic) IBOutlet UITextView *tvMovieDetail;

@end
