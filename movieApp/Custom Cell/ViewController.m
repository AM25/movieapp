//
//  ViewController.m
//  movieApp
//
//  Created by Arnita Martiana on 08/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import "ViewController.h"
#import "MovieDetailsViewController.h"
#import "CollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ViewController ()

@end

@implementation ViewController
@synthesize mColl;
@synthesize viewMovie;
@synthesize mView;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"didload");

    viewMovie.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    viewMovie.layer.borderWidth = 1.0f;
    viewMovie.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    viewMovie.layer.shadowOpacity = 0.1f;
    viewMovie.layer.masksToBounds = NO;

    
    
    // making a GET request to /init
    NSString *targetUrl = [NSString stringWithFormat:@"https://api.themoviedb.org/3/movie/popular?api_key=d7892eebd6be194bc1da917cc0d1a4e0&language=en-US&page=1"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:targetUrl]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          NSDictionary *dict = [self cleanJsonToObject:data];

          dataArray = [dict valueForKey:@"results"];
          
          NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

      }] resume];
    
    mRefreshControl = [[UIRefreshControl alloc] init];
    [mRefreshControl addTarget:self action:@selector(refreshData:)
              forControlEvents:UIControlEventValueChanged];
    [self.mColl addSubview:mRefreshControl];

    [mColl reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"viewWillAppear");
//    [self refreshData:nil];
    [self.mColl reloadData];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    NSLog(@"viewDidLayoutSubviews");
    [self.mColl reloadData];
    
}

-(void)refreshData:(NSObject*)sender{
    NSLog(@"refresh");
    if(sender==nil){
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
    
    // making a GET request to /init
    NSString *targetUrl = [NSString stringWithFormat:@"https://api.themoviedb.org/3/movie/popular?api_key=d7892eebd6be194bc1da917cc0d1a4e0&language=en-US&page=1"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:targetUrl]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          NSDictionary *dict = [self cleanJsonToObject:data];
          dataArray = [dict valueForKey:@"results"];
          
          NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
          
      }] resume];
    
    
    [mColl reloadData];
    
}

- (id)cleanJsonToObject:(id)data
{
    NSError* error;
    if (data == (id)[NSNull null])
    {
        return [[NSObject alloc] init];
    }
    id jsonObject;
    if ([data isKindOfClass:[NSData class]])
    {
        jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    } else
    {
        jsonObject = data;
    }
    if ([jsonObject isKindOfClass:[NSArray class]])
    {
        NSMutableArray *array = [jsonObject mutableCopy];
        for (int i = (int)array.count-1; i >= 0; i--)
        {
            id a = array[i];
            if (a == (id)[NSNull null])
            {
                [array removeObjectAtIndex:i];
            } else
            {
                array[i] = [self cleanJsonToObject:a];
            }
        }
        return array;
    } else if ([jsonObject isKindOfClass:[NSDictionary class]])
    {
        NSMutableDictionary *dictionary = [jsonObject mutableCopy];
        for(NSString *key in [dictionary allKeys])
        {
            id d = dictionary[key];
            if (d == (id)[NSNull null])
            {
                dictionary[key] = @"";
            } else
            {
                dictionary[key] = [self cleanJsonToObject:d];
            }
        }
        return dictionary;
    } else
    {
        return jsonObject;
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSLog(@"number:%i", dataArray.count);
    return dataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"cellSet");
    NSString *cellIdentifier = @"cellMovie";
    CollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
//    NSArray *data=[dataResponse valueForKey:@"results"];
    NSLog(@"resultsnya:%@", dataArray);
    
//    cell.lbl.text=[[dataArray valueForKey:@"title"]objectAtIndex:indexPath.row];
    [cell.lbl setText:[[dataArray valueForKey:@"title"]objectAtIndex:indexPath.row]];
    
    
    NSString *urlImg =[[dataArray valueForKey:@"poster_path"]objectAtIndex:indexPath.row];
    NSLog(@"urlImg:%@", urlImg);
    
    NSString *urlImgNew=[NSString stringWithFormat:@"http://image.tmdb.org/t/p/w500%@",urlImg];
    NSLog(@"urlImgNew:%@", urlImgNew);
    
    
    [cell.img sd_setImageWithURL:[NSURL URLWithString:urlImgNew]];
    
    cell.img.layer.cornerRadius = 10.0f;
    cell.img.layer.masksToBounds = true;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize size = CGSizeMake((mColl.frame.size.width/2)-20, 250);
    
    return size;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    curr = [dataArray objectAtIndex:indexPath.row];
    NSLog(@"curr:%@", curr);
    
    MovieDetailsViewController *secondController = [[MovieDetailsViewController alloc] init];
    secondController.theData = curr;
    
    [self performSegueWithIdentifier:@"details" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"details"]) {
        
        MovieDetailsViewController *destViewController = segue.destinationViewController;
        destViewController.theData =curr;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnPopuler:(id)sender {
    
    // making a GET request to /init
    NSString *targetUrl = [NSString stringWithFormat:@"https://api.themoviedb.org/3/movie/popular?api_key=d7892eebd6be194bc1da917cc0d1a4e0&language=en-US&page=1"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:targetUrl]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          NSDictionary *dict = [self cleanJsonToObject:data];
          NSLog(@"response : %@",dict);
          dataArray = [dict valueForKey:@"results"];
          
          NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
          NSLog(@"Data received: %@", myString);
          
          
          
          NSString *status = [dict valueForKey:@"status_code"];
          NSLog(@"status: %@", status);
          
          dataResponse = [NSMutableArray alloc];
          dataResponse = [NSMutableArray arrayWithObject:dict];
          
          
          
      }] resume];
    
    mRefreshControl = [[UIRefreshControl alloc] init];
    [mRefreshControl addTarget:self action:@selector(refreshData:)
              forControlEvents:UIControlEventValueChanged];
    [self.mColl addSubview:mRefreshControl];
    
    [mColl reloadData];
    
    mView.alpha=0;
}

- (IBAction)btnTop:(id)sender {
    // making a GET request to /init
    NSString *targetUrl = [NSString stringWithFormat:@"https://api.themoviedb.org/3/movie/top_rated?api_key=d7892eebd6be194bc1da917cc0d1a4e0&language=en-US&page=1"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:targetUrl]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          NSDictionary *dict = [self cleanJsonToObject:data];
          NSLog(@"response : %@",dict);
          dataArray = [dict valueForKey:@"results"];
          
          NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
          NSLog(@"Data received: %@", myString);
          
          
          
          NSString *status = [dict valueForKey:@"status_code"];
          NSLog(@"status: %@", status);
          
          dataResponse = [NSMutableArray alloc];
          dataResponse = [NSMutableArray arrayWithObject:dict];
          
          
          
      }] resume];
    
    mRefreshControl = [[UIRefreshControl alloc] init];
    [mRefreshControl addTarget:self action:@selector(refreshData:)
              forControlEvents:UIControlEventValueChanged];
    [self.mColl addSubview:mRefreshControl];
    
    [mColl reloadData];
    
     mView.alpha=0;
}

- (IBAction)btnNext:(id)sender {

}
- (IBAction)btnMore:(id)sender {
    NSLog(@"clicked more");
    if (mView.alpha==0) {
        mView.alpha=1;
    }else{
        mView.alpha=0;
    }
}
@end
