//
//  MovieDetailsViewController.m
//  movieApp
//
//  Created by Arnita Martiana on 08/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import "MovieDetailsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface MovieDetailsViewController ()

@end

@implementation MovieDetailsViewController
@synthesize mBtnBuy;
@synthesize mBtnBack;
@synthesize lblMovieName;
@synthesize tvMovieDetail;
@synthesize theData;
@synthesize lbRate;
@synthesize imgMovieDetail;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
    
    self.navigationController.navigationBar.backIndicatorImage = [UIImage imageNamed:@"ic_back.png"];
    self.navigationController.navigationBar.backIndicatorTransitionMaskImage = [UIImage imageNamed:@"ic_back.png"];
    
    mBtnBuy.clipsToBounds = YES;
    mBtnBuy.layer.cornerRadius = mBtnBuy.bounds.size.height/2;
    
    
    NSLog(@"data detailsnya:%@", theData);
    
    lblMovieName.text=[[theData valueForKey:@"title"]uppercaseString];
    
    tvMovieDetail.text=[theData valueForKey:@"overview"];
    lbRate.text = [NSString stringWithFormat:@"%@",[theData valueForKey:@"vote_average"]];
    
    NSString *urlImg =[theData valueForKey:@"poster_path"];

    NSString *urlImgNew=[NSString stringWithFormat:@"http://image.tmdb.org/t/p/w500%@",urlImg];
//    NSLog(@"urlImgNewDetail:%@", urlImgNew);

    
    [imgMovieDetail sd_setImageWithURL:[NSURL URLWithString:urlImgNew] placeholderImage:[UIImage imageNamed:@"movie.png"] options:SDWebImageRefreshCached];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
