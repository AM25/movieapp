//
//  CollectionViewCell.h
//  movieApp
//
//  Created by Arnita Martiana on 10/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img;

@property (strong, nonatomic) IBOutlet UILabel *lbl;

@end
