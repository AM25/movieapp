//
//  AppDelegate.h
//  movieApp
//
//  Created by Arnita Martiana on 08/10/18.
//  Copyright © 2018 Arnita Martiana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{

@public
UINavigationController *mIntroNavigationController;
UIViewController *mLoginViewController;
    
}

@property (strong, nonatomic) UIWindow *window;


@end

